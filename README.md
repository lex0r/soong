# Soong\Soong

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Soong is a framework for building robust Extract-Transform-Load (ETL) applications for performing data migration. It is designed to be record-oriented and configuration-driven - most applications will require little or no custom PHP code, and tools can easily customize (or create) data migration processes implemented by Soong.

Documentation is at https://soong-etl.readthedocs.io/.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html). For major version 0, we will increment the minor version for backward-incompatible changes. At this pre-release point, the interfaces are still changing regularly - if you develop any applications using Soong be sure to pin them to the minor release.

soong/soong began as a standalone repository, containing all components developed for soong so far as well as sample migrations (see **Demos** below). The component implementations are now being broken out into separate smaller libraries - soong/soong itself may end up containing only the interfaces (contracts), or (more likely) also basic implementations for the most widely-needed components.

## Install

Soong is best installed using [Composer](https://getcomposer.org/). Since as noted above while in major version 0 minor versions will contain backward-incompatible changes, and at this point the interfaces are still changing regularly, if you develop any applications using Soong be sure to pin them to the minor release you implemented them with. E.g., "~0.5.0" which will get the latest release with major.minor version 0.5 and prevent updating to a 0.6.x release which may break your application.

``` bash
$ composer require soong/soong "~0.5.0"
```

## Change log

Please see [CHANGELOG](docs/CHANGELOG.md) for more information on what has changed recently.

## Contributing

There's still a lot of refinement to be done to Soong - this is your opportunity to get involved with a new framework (and community) on the ground floor! As mentioned above, the plan is ultimately to break out components into small well-contained libraries - these will be excellent opportunities to get your feet wet maintaining your own open-source project.

Please see [CONTRIBUTING](docs/CONTRIBUTING.md) and [CODE_OF_CONDUCT](docs/CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email `soong@virtuoso-performance.com` instead of using the issue tracker.

## Credits

- [Mike Ryan][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/soong/soong.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/soong/soong/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/gl/soong/soongetl/soong.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/quality/gl/soong/soongetl/soong.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/soong/soong.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/soong/soong
[link-scrutinizer]: https://scrutinizer-ci.com/gl/soong/soongetl/soong/code-structure
[link-code-quality]: https://scrutinizer-ci.com/gl/soong/soongetl/soong/
[link-downloads]: https://packagist.org/packages/soong/soong
[link-author]: https://gitlab.com/mikeryan
[link-contributors]: ../../contributors
