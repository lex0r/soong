<?php
declare(strict_types=1);

namespace Soong\Contracts\Task;

use Soong\Contracts\Extractor\Extractor;
use Soong\Contracts\KeyMap\KeyMap;
use Soong\Contracts\Loader\Loader;
use Soong\Contracts\Transformer\RecordTransformer;

/**
 * Tasks for performing migration operations.
 */
interface EtlTask extends Task
{

    /**
     * Retrieves the configured extractor for this task, if any.
     *
     * @return Extractor|null The extractor, or NULL if none.
     */
    public function getExtractor() : ?Extractor;

    /**
     * Retrieves the configured record transformer for this task, if any.
     *
     * @return RecordTransformer[]|null Array of the record transformers, or NULL if none.
     */
    public function getTransformers() : ?array;

    /**
     * Retrieves the configured loader for this task, if any.
     *
     * @return Loader|null The loader, or NULL if none.
     */
    public function getLoader() : ?Loader;

    /**
     * Retrieves the configured keymap for this task, if any.
     *
     * @return KeyMap|null The keymap, or NULL if none.
     */
    public function getKeyMap() : ?KeyMap;
}
