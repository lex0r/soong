<?php
declare(strict_types=1);

namespace Soong\Contracts\Task;

use Soong\Contracts\Configuration\ConfigurableComponent;

/**
 * Interface for task definitions.
 */
interface Task extends ConfigurableComponent
{
    /**
     * ID of a task.
     * @return string
     */
    public function getId(): string;

    /**
     * Fully-qualified ID of a task (includes migration name).
     * @return string
     */
    public function getFQId(): string;

    /**
     * Format fully-qualified ID of a task using migration name and task name.
     * @param string $migration
     * @param string $task
     * @return string
     */
    public static function formatFQId(string $migration, string $task): string;
}
