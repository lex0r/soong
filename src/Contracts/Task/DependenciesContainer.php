<?php
declare(strict_types=1);

namespace Soong\Contracts\Task;

use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Psr16Cache;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface for a simple container of dependencies.
 */
interface DependenciesContainer
{
    public function getStdin() : ?InputInterface;

    public function getStdout() : ?OutputInterface;

    public function getCache() : ?Psr16Cache;

    public function getLogger() : ?LoggerInterface;

    public function setStdin(InputInterface $stdin) : void;

    public function setStdout(OutputInterface $stdout) : void;

    public function setCache(Psr16Cache $cache) : void;

    public function setLogger(LoggerInterface $logger) : void;
}
