<?php

namespace Soong\Contracts\Extractor;

interface ExtendedTaskStatusProvider
{
    public function getExtractedKeysValues(): array;
}
