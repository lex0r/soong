<?php

namespace Soong\Contracts\KeyMap;

interface ExtendedTaskStatusProvider
{
    public function getExtractedKeysValues(): array;
}
