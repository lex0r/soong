<?php
declare(strict_types=1);

namespace Soong\Contracts\Data;

/**
 * Collection of named data properties and values.
 */
interface RecordPayload
{

    /**
     * Retrieves the migration source record.
     */
    public function getSourceRecord() : Record;

    /**
     * Retrieves the migration destination record.
     */
    public function getDestinationRecord() : Record;

    /**
     * Replaces the migration destination record.
     */
    public function setDestinationRecord(Record $record) : void;
}
