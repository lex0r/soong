<?php

declare(strict_types=1);

namespace Soong\Transformer\Record;

use Soong\Configuration\OptionsResolverComponent;
use Soong\Contracts\Data\RecordPayload;
use Soong\Contracts\KeyMap\KeyMap;
use Soong\Contracts\Transformer\RecordTransformer;
use Soong\DBAL\Loader\DBALMergeLoader;
use Soong\Task\SimpleTaskContainer;

abstract class RecordTransformerBase extends OptionsResolverComponent implements RecordTransformer
{
    protected RecordPayload $currentPayload;

    /**
     * @param RecordPayload $payload
     * @return RecordPayload
     */
    public function __invoke(RecordPayload $payload): RecordPayload
    {
        $this->currentPayload = $payload;
        return $payload;
    }

    /**
     * @param string $property
     * @return mixed
     */
    protected function getSourceProperty(string $property): mixed
    {
        return $this->currentPayload->getSourceRecord()->getPropertyValue($property);
    }

    /**
     * @return array
     */
    protected function getSourceProperties(): array
    {
        return $this->currentPayload->getSourceRecord()->toArray();
    }

    /**
     * @param string $property
     * @return mixed
     */
    protected function getDestinationProperty(string $property): mixed
    {
        return $this->currentPayload->getDestinationRecord()->getPropertyValue($property);
    }

    /**
     * @return array
     */
    protected function getDestinationProperties(): array
    {
        return $this->currentPayload->getDestinationRecord()->toArray();
    }

    /**
     * @param string $property
     * @param mixed $value
     * @return void
     */
    protected function setDestinationProperty(string $property, mixed $value): void
    {
        $this->currentPayload->getDestinationRecord()->setPropertyValue($property, $value);
    }

    /**
     * Check if current record was previously processed.
     * @return bool True if the record was earlier processed and false otherwise.
     */
    protected function isProcessed(): bool
    {
        $task = SimpleTaskContainer::instance()->getActiveTask();
        $extractor = $task->getExtractor();
        $keyMap = $task->getKeyMap();
        $sourceKeyNames = array_keys($extractor->getKeyProperties());
        $sourceKeys = [];
        foreach ($sourceKeyNames as $keyName) {
            $sourceKeys[] = $this->getSourceProperty($keyName);
        }
        $loadedKeys = $keyMap->lookupLoadedKey($sourceKeys);
        if (!empty($loadedKeys)) {
            return true;
        }
        return false;
    }

    /**
     * Update destination record's keys in case the records was previously processed.
     * @return bool True if the record's keys were updated (and the record pre-existed) or false otherwise.
     */
    protected function updateDestinationKey(): bool
    {
        /** @var \Soong\Contracts\Task\EtlTask $task */
        $task = SimpleTaskContainer::instance()->getActiveTask();
        $extractor = $task->getExtractor();
        $keyMap = $task->getKeyMap();
        $loader = $task->getLoader();
        // Only update if DBALMergeLoader is used because it uses this key to match against existing records to
        // perform either an insert or an update.
        if (!$loader instanceof DBALMergeLoader) {
            return false;
        }
        $sourceKeyNames = array_keys($extractor->getKeyProperties());
        $sourceKeys = [];
        foreach ($sourceKeyNames as $keyName) {
            $sourceKeys[] = $this->getSourceProperty($keyName);
        }
        $loadedKeys = $keyMap->lookupLoadedKey($sourceKeys);
        if (!empty($loadedKeys)) {
            foreach ($loadedKeys as $keyName => $keyValue) {
                $this->setDestinationProperty($keyName, $keyValue);
            }
            return true;
        }
        return false;
    }

    /**
     *  Lookup source keys using destination (loaded) keys. A helper method useful when dealing with duplicates.
     */
    protected function fetchSourceKeys($destinationKeys)
    {
        /** @var \Soong\Contracts\Task\EtlTask $task */
        $task = SimpleTaskContainer::instance()->getActiveTask();
        $keyMap = $task->getKeyMap();
        return $keyMap->lookupExtractedKeys($destinationKeys);
    }
}
