<?php

namespace Soong\Logger;

use Psr\Log\LogLevel;
use Soong\Contracts\Extractor\Extractor;
use Soong\Contracts\Loader\Loader;
use Soong\Contracts\Transformer\PropertyTransformer;
use Soong\Contracts\Transformer\RecordTransformer;
use Soong\Task\SimpleTaskContainer;
use Symfony\Component\Console\Logger\ConsoleLogger;

trait Logger
{
    public function log($level, $message, array $context = [], array $options = []): void
    {
        $prefixes = $this->getPrefixes($options);
        $levelPadding = $this->getLevelPadding($level);
        /** @var ConsoleLogger $logger */
        $logger = SimpleTaskContainer::instance()->getLogger();
        if ($logger) {
            $logger->log($level, "$levelPadding $prefixes $message", $context);
        } else {
            throw new \Exception("Logging is not possible, make sure the PSR Logger interface is avaialable");
        }
    }

    public function getPrefixes(array $options = []): string
    {
        $memoryUsagePrefix = !empty($options['memory_usage']) ? $this->getMemoryUsagePrefix() : '';
        $timestampPrefix = !empty($options['timestamp']) ? $this->getTimeStampPrefix($options['timestamp_format'] ?? null) : '';
        if ($this instanceof Extractor) {
            $etlPrefix = '[EX]';
        } elseif ($this instanceof PropertyTransformer) {
            $etlPrefix = '[PT]';
        } elseif ($this instanceof RecordTransformer) {
            $etlPrefix = '[RT]';
        } elseif ($this instanceof Loader) {
            $etlPrefix = '[LD]';
        } elseif (!empty($options['etl_prefix'])) {
            $etlPrefix = (string) $options['etl_prefix'];
        } else {
            $etlPrefix = '';
        }
        $prefixes = [];
        if ($timestampPrefix) {
            $prefixes[] = $timestampPrefix;
        }
        if ($etlPrefix) {
            $prefixes[] = $etlPrefix;
        }
        if ($memoryUsagePrefix) {
            $prefixes[] = $memoryUsagePrefix;
        }
        return implode(' ', $prefixes);
    }

    protected function getMemoryUsagePrefix()
    {
        $size = memory_get_peak_usage(true);
        $sizeString = $this->formatBytes($size);
        $sizeString = str_pad($sizeString, 9, ' ', STR_PAD_LEFT);
        return '[' . $sizeString . ']';
    }

    protected function getTimeStampPrefix(?string $format = null)
    {
        $format = $format ?? 'Y-m-d H:i:s';
        return '[' . date($format) . ']';
    }

    private function getLevelPadding($logLevel): string
    {
        $maxLength = strlen(LogLevel::CRITICAL);
        return str_repeat(' ', $maxLength - strlen($logLevel));
    }

    public static function formatBytes(int $bytes)
    {
        if ($bytes > 1024 * 1024 * 1024) {
            return round($bytes / 1024 / 1024 / 1024, 3).' GB';
        } elseif ($bytes > 1024 * 1024) {
            return round($bytes / 1024 / 1024, 2).' MB';
        } elseif ($bytes > 1024) {
            return round($bytes / 1024, 2).' KB';
        }
        return $bytes . ' B';
    }
}
