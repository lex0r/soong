<?php

declare(strict_types=1);

namespace Soong\Extractor;

trait ExtendedTaskStatusProviderImpl
{
    protected array $extractedKeysValues = [];

    public function getExtractedKeysValues(): array
    {
        return $this->extractedKeysValues;
    }

    private function rememberKeysValues(int|string $index, array $data): void
    {
        $keys = array_keys($this->getKeyProperties());
        foreach ($keys as $key) {
            $this->extractedKeysValues[$index] ??= [];
            $this->extractedKeysValues[$index][$key] = $data[$key];
        }
    }
}
