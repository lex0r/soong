<?php

declare(strict_types=1);

namespace Soong\Task;

use League\Pipeline\Pipeline;
use Psr\Log\LogLevel;
use Soong\Console\ControlCommand\StopMigrationCommand;
use Soong\Contracts\Data\RecordFactory;
use Soong\Contracts\Data\RecordPayload;
use Soong\Contracts\Task\EtlTask;
use Soong\Contracts\Transformer\RecordTransformer;
use Soong\Data\BasicRecordPayload;
use Soong\Logger\Logger;

class MigrateOperation extends EtlTaskOperation
{
    use Logger {
        log as protected doLog;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(TaskPayload $taskPayload): TaskPayload
    {
        set_time_limit(0);
        $taskContainer = SimpleTaskContainer::instance();
        $taskContainer->setActiveTask($this->task);
        $task = $this->task;
        $cache = $taskContainer->getCache();
        if (empty($task->getExtractor())) {
            // @todo Throw a TaskException.
            return $taskPayload;
        }
        $offset = intval($taskPayload->getOptions()['offset'] ?? 1);
        $limit = intval($taskPayload->getOptions()['limit'] ?? 0);
        $batch = intval($taskPayload->getOptions()['batch']) ?: null;
        $recordFactory = $task->getConfigurationValue('record_factory');
        $processedCount = 0;
        $skipMessageShown = false;
        $stopCommand = new StopMigrationCommand($task->getFQId(), $taskContainer->getCache());
        $itemRangeDetails = $this->getItemRangeDetails($offset, $limit, $batch);
        $this->log(LogLevel::NOTICE, "<info>Starting task '{$task->getId()}'. $itemRangeDetails</info>");
        foreach ($task->getExtractor()->extractFiltered() as $index => $sourceData) {
            if ($stopCommand->wasSent()) {
                $this->log(
                    LogLevel::WARNING,
                    "<comment>The migration was requested to stop. "
                    . "Last processed index: $index. Total processed: $processedCount</comment>"
                );
                $stopCommand->markAsExecuted();
                $cache->set(self::getMigrateOperationResultKey(), ['processed' => $processedCount]);
                return $taskPayload;
            }
            if ($offset > 1 && ($index + 1) < $offset) {
                if (!$skipMessageShown) {
                    $this->log(LogLevel::NOTICE, "<info>Skipping the first " . ($offset - 1) . " items</info>");
                    $skipMessageShown = true;
                }
                continue;
            }
            try {
                $this->process($recordFactory, $task, $sourceData);
            } catch (\Exception $e) {
                $exceptionDetails = $this->formatException($e);
                /**
                 * When processing a batch avoid raising errors above WARNING as stderr will be used for those and this
                 * is not supported as the logger uses stdout and doesn't redirect stderr to stdout. Add 'critical'
                 * label to avoid losing the importance of the message.
                 * @todo Redirect stderr > stdout?
                 */
                if ($batch) {
                    $this->log(LogLevel::WARNING, '[critical] ' . $exceptionDetails);
                } else {
                    $this->log(LogLevel::CRITICAL, $exceptionDetails);
                }
                $this->log(LogLevel::WARNING, "<info>Processing interrupted by a critical error at index " . ($index + 1) . ". Total processed: $processedCount.</info>");
                break;
            }
            $processedCount++;
            // Limit is hit.
            if ($limit && $processedCount >= $limit) {
                $this->log(LogLevel::NOTICE, "<info>Finished processing at index " . ($index + 1) . ". Total processed: $processedCount.</info>");
                break;
            }
        }
        $this->log(LogLevel::NOTICE, "<info>Finished task '{$task->getId()}'</info>");
        $cache->set(self::getMigrateOperationResultKey(), ['processed' => $processedCount]);
        return $taskPayload;
    }

    /**
     * @param RecordPayload $recordPayload
     * @param EtlTask $task
     */
    private function saveKeyMap(RecordPayload $recordPayload, EtlTask $task): void
    {
        $sourceRecordValues = $recordPayload->getSourceRecord()->toArray();
        $extractedKey = array_map(
            function ($key) use ($sourceRecordValues) {
                return $sourceRecordValues[$key];
            },
            array_keys($task->getExtractor()->getKeyProperties())
        );
        $destinationRecordValues = $recordPayload->getDestinationRecord()->toArray();
        $loadedKey = array_map(
            function ($key) use ($destinationRecordValues) {
                return $destinationRecordValues[$key];
            },
            array_keys($task->getLoader()->getKeyProperties())
        );
        $task->getKeyMap()->saveKeyMap($extractedKey, $loadedKey);
    }

    /**
     * @param RecordFactory $recordFactory
     * @param EtlTask $task
     * @param mixed $sourceData
     * @return void
     */
    private function process(RecordFactory $recordFactory, EtlTask $task, mixed $sourceData): void
    {
        $destinationData = $recordFactory->create();
        $pipeline = new Pipeline();
        /** @var RecordTransformer $recordTransformer */
        foreach ($task->getTransformers() as $recordTransformer) {
            $pipeline = $pipeline->pipe($recordTransformer);
        }
        $pipeline = $pipeline->pipe($task->getLoader());
        /** @var RecordPayload $recordPayload */
        $recordPayload = $pipeline->process(new BasicRecordPayload($sourceData, $destinationData));
        if (!is_null($task->getKeyMap())) {
            $this->saveKeyMap($recordPayload, $task);
        }
    }

    /**
     * @return string
     */
    public static function getMigrateOperationResultKey(): string
    {
        return 'migrate_operation_last_result';
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return string
     */
    private function getItemRangeDetails(int $offset, int $limit, ?int $batch): string
    {
        if ($offset == 1 && $limit == 0) {
            return "Processing all items";
        } elseif ($limit == 0) {
            return "Processing all items starting with $offset";
        } else {
            return "Processing indexes $offset - " . ($offset + $limit - 1) . ($batch ? " (batch $batch)" : "");
        }
    }

    /**
     * @param $exception
     * @return string
     */
    private function formatException($exception): string
    {
        $previousMessage = $exception->getPrevious()?->getMessage();
        $result = $exception->getMessage() . ($previousMessage ? " (previous exception: $previousExceptionMessage)" : "");
        $result .= "\nStack trace:\n" . $exception->getTraceAsString();
        return $result;
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     * @return void
     */
    private function log($level, $message, array $context = []): void
    {
        $this->doLog($level, $message, $context, ['memory_usage' => true, 'timestamp' => true, 'etl_prefix' => '[MO]']);
    }
}
