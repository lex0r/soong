<?php

declare(strict_types=1);

namespace Soong\Task;

use Psr\Log\LogLevel;
use Soong\Logger\Logger;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class MigrateBatchesOperation extends EtlTaskOperation
{
    use Logger {
        log as protected doLog;
    }

    /**
     * @inheritDoc
     */
    public function __invoke(TaskPayload $taskPayload): TaskPayload
    {
        set_time_limit(0);
        $taskContainer = SimpleTaskContainer::instance();
        $input = $taskContainer->getStdin();
        $output = $taskContainer->getStdout();
        $cache = $taskContainer->getCache();
        $batchSize = intval($input->getOption('batch_size'));
        $offset = intval($input->getOption('offset') ?? 1);
        $totalProcessed = 0;
        $batch = 1;
        while (true) {
            $newOffset = $offset + $batchSize;
            $invocationParams = $this->getCliInvocationParams($input, $offset, $batch++);
            $invocationParams = $this->addVerbosityParam($invocationParams, $output);
            $migrateCommandProcess = $this->invokeMigrateCommand($invocationParams);
            while ($migrateCommandProcess->isRunning() && !$migrateCommandProcess->isTerminated()) {
                $this->copyProcessOutput($migrateCommandProcess, $output);
                usleep(10000);
            }
            $moResult = $cache->get(MigrateOperation::getMigrateOperationResultKey());
            if (!$moResult) {
                throw new \RuntimeException(__CLASS__ . ': No result was returned by the most recent migration batch');
            }
            $processed = $moResult['processed'];
            $totalProcessed += $processed;
            // If less than batch size items were migrated it means either there are no more items to migrate
            // or the above operation was interrupted. Both cases are loop break conditions.
            if ($processed < $batchSize) {
                $this->log(LogLevel::NOTICE, "<info>Finished migration using batches. Total processed: $totalProcessed</info>");
                $cache->delete(MigrateOperation::getMigrateOperationResultKey());
                break;
            }
            $offset = $newOffset;
        }
        return $taskPayload;
    }

    /**
     * @param array $inputs
     * @return Process
     */
    private function invokeMigrateCommand(array $inputs): Process
    {
        $process = new Process(
            array_merge(
                [env('SOONG_PHP_BINARY_PATH', 'php'), 'artisan', 'soong:migrate'], $inputs
            )
        );
        $process->setTimeout(null)->setIdleTimeout(null);
        if (env('APP_DEBUG', false)) {
            $process->setEnv(['XDEBUG_CONFIG'=>'idekey=phpstorm']);
        }
        $process->start();
        // Normally, by the time the below code is executed the process is already running, but we have the below
        // check in case the system is slow on starting new processes.
        $this->throwIfProcessNotRunning($process);
        return $process;
    }

    /**
     * @param InputInterface $input
     * @param int $offset
     * @return string[]
     */
    private function getCliInvocationParams(InputInterface $input, int $offset, int $batch): array
    {
        $directoryNames = $input->getOption('directory');
        $batchSize = (int) $input->getOption('batch_size');
        $invocationParams = [];
        foreach ($directoryNames as $directoryName) {
            $invocationParams[] = "--directory=$directoryName";
        }
        $invocationParams[] = "--offset=$offset";
        $invocationParams[] = "--limit=$batchSize";
        $invocationParams[] = "--batch=$batch";
        $invocationParams[] = $this->task->getId();
        return $invocationParams;
    }

    /**
     * @param array $invocationParams
     * @param OutputInterface $output
     * @return array
     */
    private function addVerbosityParam(array $invocationParams, OutputInterface $output): array
    {
        if ($output->isDebug()) {
            $invocationParams[] = '-vvv';
        } elseif ($output->isVeryVerbose()) {
            $invocationParams[] = '-vv';
        } elseif ($output->isVerbose()) {
            $invocationParams[] = '-v';
        }
        return $invocationParams;
    }

    /**
     * @param Process $process
     * @return void
     */
    private function throwIfProcessNotRunning(Process $process): void
    {
        $waitingTime = 1000;
        $checkInterval = 100;
        $checksToDo = $waitingTime / $checkInterval;
        while (!$process->isRunning() && $checksToDo) {
            usleep($checkInterval);
            $checksToDo--;
        }
        if (!$process->isRunning()) {
            throw new \RuntimeException(__CLASS__ . ": Failed to confirm the process is running after $waitingTime microseconds.");
        }
    }

    /**
     * @param Process $process
     * @param OutputInterface $output
     * @return void
     */
    private function copyProcessOutput(Process $process, OutputInterface $output)
    {
        $result = $process->getIncrementalOutput();
        if (!$result) {
            return;
        }
        foreach (array_filter(explode("\n", $result)) as $line) {
            $output->writeln($line);
        }
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     * @return void
     */
    private function log($level, $message, array $context = []): void
    {
        $this->doLog($level, $message, $context, ['memory_usage' => true, 'timestamp' => true, 'etl_prefix' => '[BO]']);
    }
}
