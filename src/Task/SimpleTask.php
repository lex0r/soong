<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Configuration\OptionsResolverComponent;
use Soong\Contracts\Task\Task;
use Soong\Contracts\Task\TaskContainer;

/**
 * Basic base class for migration tasks.
 */
class SimpleTask extends OptionsResolverComponent implements Task
{
    private const FQN_PARTS_SEPARATOR = '____';

    public function __construct(array $configuration, protected string $taskFQId)
    {
        parent::__construct($configuration);
    }

    /**
     * @inheritdoc
     */
    public function getId(): string
    {
        return explode(self::FQN_PARTS_SEPARATOR, $this->taskFQId)[1];
    }

    /**
     * @inheritdoc
     */
    public function getFQId(): string
    {
        return $this->taskFQId;
    }

    /**
     * @inheritdoc
     */
    public static function formatFQId(string $migration, string $task): string
    {
        return $migration . self::FQN_PARTS_SEPARATOR . $task;
    }

    /**
     * @inheritdoc
     */
    protected function optionDefinitions(): array
    {
        $options = parent::optionDefinitions();
        $options['container'] = [
            'required' => true,
            'allowed_types' => TaskContainer::class,
        ];
        return $options;
    }
}
