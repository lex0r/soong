<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Contracts\Task\EtlTask;
use Soong\Contracts\Task\Operation;

abstract class EtlTaskOperation implements Operation
{

    public function __construct(protected EtlTask $task)
    {
    }
}
