<?php
declare(strict_types=1);

namespace Soong\Task;

use Psr\Log\LoggerInterface;
use Soong\Contracts\Exception\ComponentNotFound;
use Soong\Contracts\Task\DependenciesContainer;
use Soong\Contracts\Task\Task;
use Soong\Contracts\Task\TaskContainer;
use Symfony\Component\Cache\Psr16Cache;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Basic base class for migration tasks.
 */
class SimpleTaskContainer implements TaskContainer, DependenciesContainer
{
    private ?InputInterface $stdin = null;

    private ?OutputInterface $stdout = null;

    private ?Psr16Cache $cache = null;

    private ?LoggerInterface $logger = null;

    /**
     * @var SimpleTaskContainer
     */
    private static self $instance;

    /**
     * @internal
     *
     * All known tasks, keyed by id.
     *
     * @var SimpleTask[] $tasks
     */
    protected array $tasks = [];

    /**
     * @internal
     * Task being executed.
     * @var Task
     */
    protected ?Task $activeTask = null;

    /**
     * @internal
     *
     * All registered dependencies, keyed by id.
     *
     * @var object[] $dependencies
     */
    protected array $dependencies = [];

    private function __construct() {}

    public static function instance(bool $new = false)
    {
        if (!$new && isset(self::$instance)) {
            return self::$instance;
        }
        return self::$instance = new self();
    }

    /**
     * @inheritdoc
     */
    public function add(string $id, Task $task) : void
    {
        $this->tasks[$id] = $task;
    }

    /**
     * @inheritdoc
     */
    public function get(string $id): Task
    {
        if (empty($this->tasks[$id])) {
            throw new ComponentNotFound("Task $id not found.");
        }
        return $this->tasks[$id];
    }

    /**
     * @inheritdoc
     */
    public function getAll(): array
    {
        return $this->tasks;
    }

    /**
     * @inheritdoc
     */
    public function getActiveTask(): ?Task
    {
        return $this->activeTask;
    }

    /**
     * @inheritdoc
     */
    public function setActiveTask(Task $task): void
    {
        $this->activeTask = $task;
    }

    public function getStdin(): ?InputInterface
    {
        return $this->stdin;
    }

    public function getStdout(): ?OutputInterface
    {
        return $this->stdout;
    }

    public function getCache(): ?Psr16Cache
    {
        return $this->cache;
    }

    public function getLogger(): ?LoggerInterface
    {
        return $this->logger;
    }

    public function setStdin(InputInterface $stdin): void
    {
        $this->stdin = $stdin;
    }

    public function setStdout(OutputInterface $stdout): void
    {
        $this->stdout = $stdout;
    }

    public function setCache(Psr16Cache $cache): void
    {
        $this->cache = $cache;
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }
}
