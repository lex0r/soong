<?php
declare(strict_types=1);

namespace Soong\Data;

use Soong\Contracts\Data\Record;
use Soong\Contracts\Data\RecordPayload;

/**
 * Basic implementation of data records as arrays.
 */
class BasicRecordPayload implements RecordPayload
{

    /**
     * BasicRecordPayload constructor.
     */
    public function __construct(protected Record $sourceRecord, protected Record $destinationRecord)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getSourceRecord(): Record
    {
        return $this->sourceRecord;
    }

    /**
     * {@inheritDoc}
     */
    public function getDestinationRecord(): Record
    {
        return $this->destinationRecord;
    }

    /**
     * {@inheritDoc}
     */
    public function setDestinationRecord(Record $record): void
    {
        $this->destinationRecord = $record;
    }
}
