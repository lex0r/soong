<?php

namespace Soong\Tests\Task;

use Soong\Task\SimpleTaskContainer;
use Soong\Tests\Contracts\Task\TaskContainerTestBase;

/**
 * Tests the \Soong\Task\SimpleTaskContainer class.
 */
class TaskContainerTest extends TaskContainerTestBase
{

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->taskContainerClass = '\\' . SimpleTaskContainer::class;
    }
}
