<?php

namespace Soong\Tests\Data;

use Soong\Data\BasicRecordFactory;
use Soong\Tests\Contracts\Data\RecordTestBase;

/**
 * Tests the \Soong\Data\Record class.
 */
class RecordTest extends RecordTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->recordFactoryClass = '\\' . BasicRecordFactory::class;
    }
}
