<?php

namespace Soong\Tests\Contracts\Filter;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Data\Record;
use Soong\Contracts\Filter\Filter;

/**
 * Base class for testing Filter implementations.
 *
 * To test a filter class, extend this class and implement setUp(),
 * assigning the fully-qualified class name to filterClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->filterClass = '\Soong\Filter\Select';
 *     }
 * @endcode
 *
 * And implement filterDataProvider(), where each row of data provided contains
 * a data array to test against, a configuration array for the filter, and the
 * expected result:
 *
 * @code
 *     public function filterDataProvider()
 *     {
 *         return [
 *             '= int passes' => [
 *                 ['intprop1' => 11, 'stringprop1' => ''],
 *                 ['criteria' => [['intprop1', '=', 11]]],
 *                 true,
 *             ],
 *             '= int fails' => [
 *                 ['intprop1' => 12, 'stringprop1' => ''],
 *                 ['criteria' => [['intprop1', '=', 11]]],
 *                 false,
 *             ],
 *             '= string passes' => [
 *                 ['intprop1' => 13, 'stringprop1' => 'a string'],
 *                 ['criteria' => [['stringprop1', '=', 'a string']]],
 *                 true,
 *             ],
 *         ],
 *     }
 * @endcode
 */
abstract class FilterTestBase extends TestCase
{

    /**
     * Fully-qualified name of a Filter implementation.
     *
     * @var Filter $filterClass
     */
    protected $filterClass;

    /**
     * Test filter().
     *
     * @dataProvider filterDataProvider
     *
     * @param array $sourceData
     *   Source data to be tested.
     * @param array $filterConfiguration
     *   Configured filter criteria.
     * @param bool $expected
     *   Expected result of the transformation.
     */
    public function testFilter(array $sourceData, array $filterConfiguration, bool $expected) : void
    {
        $record = $this->createMock(Record::class);
        $map = [];
        foreach ($sourceData as $name => $value) {
            $map[] = [$name, $value];
        }
        $record->method('getPropertyValue')
            ->will($this->returnValueMap($map));
        /** @var \Soong\Contracts\Filter\Filter $filter */
        $filter = new $this->filterClass($filterConfiguration);
        /** @var Record $record */
        $this->assertEquals($expected, $filter->filter($record), "{$this->filterClass} filtered correctly");
    }
}
