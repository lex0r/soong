<?php

namespace Soong\Tests\Filter;

use Soong\Contracts\Data\Record;
use Soong\Contracts\Exception\UnrecognizedOperator;
use Soong\Filter\Select;
use Soong\Tests\Contracts\Filter\FilterTestBase;

/**
 * Tests the \Soong\Filter\Select class.
 */
class SelectTest extends FilterTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->filterClass = '\\' . Select::class;
    }

    /**
     * Test filtering various scenarios.
     */
    public function filterDataProvider() : array
    {
        $row1 = [
            'intprop1' => 11,
            'intprop2' => -35,
            'stringprop1' => 'a string',
            'stringprop2' => 'another string',
            'stringint' => '58',
        ];
        return [
            '= int passes' => [
                $row1,
                ['criteria' => [['intprop1', '=', 11]]],
                true,
            ],
            '= int fails' => [
                $row1,
                ['criteria' => [['intprop1', '=', 12]]],
                false,
            ],
            '= string passes' => [
                $row1,
                ['criteria' => [['stringprop1', '=', 'a string']]],
                true,
            ],
            '= string fails' => [
                $row1,
                ['criteria' => [['stringprop1', '=', 'not the string']]],
                false,
            ],
            '== int passes' => [
                $row1,
                ['criteria' => [['intprop1', '==', 11]]],
                true,
            ],
            '== int fails' => [
                $row1,
                ['criteria' => [['intprop1', '==', 12]]],
                false,
            ],
            '== string passes' => [
                $row1,
                ['criteria' => [['stringprop1', '==', 'a string']]],
                true,
            ],
            '== string fails' => [
                $row1,
                ['criteria' => [['stringprop1', '==', 'not the string']]],
                false,
            ],
            '== int/string passes' => [
                $row1,
                ['criteria' => [['intprop2', '==', '-35']]],
                true,
            ],
            '== int/string fails' => [
                $row1,
                ['criteria' => [['intprop2', '==', '-34']]],
                false,
            ],
            '== string/int passes' => [
                $row1,
                ['criteria' => [['stringint', '==', 58]]],
                true,
            ],
            '== string/int fails' => [
                $row1,
                ['criteria' => [['stringint', '==', 57]]],
                false,
            ],
            '!= int passes' => [
                $row1,
                ['criteria' => [['intprop2', '!=', -34]]],
                true,
            ],
            '!= int fails' => [
                $row1,
                ['criteria' => [['intprop2', '!=', -35]]],
                false,
            ],
            '!= string passes' => [
                $row1,
                ['criteria' => [['stringprop1', '!=', 'astring']]],
                true,
            ],
            '!= string fails' => [
                $row1,
                ['criteria' => [['stringprop1', '!=', 'a string']]],
                false,
            ],
            '<> int passes' => [
                $row1,
                ['criteria' => [['intprop2', '<>', -34]]],
                true,
            ],
            '<> int fails' => [
                $row1,
                ['criteria' => [['intprop2', '<>', -35]]],
                false,
            ],
            '<> string passes' => [
                $row1,
                ['criteria' => [['stringprop1', '<>', 'astring']]],
                true,
            ],
            '<> string fails' => [
                $row1,
                ['criteria' => [['stringprop1', '<>', 'a string']]],
                false,
            ],
            '=== int/string fails' => [
                $row1,
                ['criteria' => [['intprop2', '===', '-35']]],
                false,
            ],
            '=== string/int fails' => [
                $row1,
                ['criteria' => [['stringint', '===', 58]]],
                false,
            ],
            '=== int passes' => [
                $row1,
                ['criteria' => [['intprop1', '===', 11]]],
                true,
            ],
            '=== int fails' => [
                $row1,
                ['criteria' => [['intprop1', '===', 12]]],
                false,
            ],
            '=== string passes' => [
                $row1,
                ['criteria' => [['stringprop1', '===', 'a string']]],
                true,
            ],
            '=== string fails' => [
                $row1,
                ['criteria' => [['stringprop1', '===', 'not the string']]],
                false,
            ],
            '!== int/string fails' => [
                $row1,
                ['criteria' => [['intprop2', '!==', '-35']]],
                true,
            ],
            '!== string/int fails' => [
                $row1,
                ['criteria' => [['stringint', '!==', 58]]],
                true,
            ],
            '!== int passes' => [
                $row1,
                ['criteria' => [['intprop1', '!==', 11]]],
                false,
            ],
            '!== int fails' => [
                $row1,
                ['criteria' => [['intprop1', '!==', 12]]],
                true,
            ],
            '!== string passes' => [
                $row1,
                ['criteria' => [['stringprop1', '!==', 'a string']]],
                false,
            ],
            '!== string fails' => [
                $row1,
                ['criteria' => [['stringprop1', '!==', 'not the string']]],
                true,
            ],
            '< int passes' => [
                $row1,
                ['criteria' => [['intprop1', '<', 12]]],
                true,
            ],
            '< int fails' => [
                $row1,
                ['criteria' => [['intprop1', '<', 10]]],
                false,
            ],
            '< int fails when equal' => [
                $row1,
                ['criteria' => [['intprop1', '<', 11]]],
                false,
            ],
            '< string passes' => [
                $row1,
                ['criteria' => [['stringprop2', '<', 'bnother string']]],
                true,
            ],
            '< string fails' => [
                $row1,
                ['criteria' => [['stringprop2', '<', 'aaother string']]],
                false,
            ],
            '< string fails when equal' => [
                $row1,
                ['criteria' => [['stringprop2', '<', 'another string']]],
                false,
            ],
            '<= int passes' => [
                $row1,
                ['criteria' => [['intprop1', '<=', 12]]],
                true,
            ],
            '<= int fails' => [
                $row1,
                ['criteria' => [['intprop1', '<=', 10]]],
                false,
            ],
            '<= int passes when equal' => [
                $row1,
                ['criteria' => [['intprop1', '<=', 11]]],
                true,
            ],
            '<= string passes' => [
                $row1,
                ['criteria' => [['stringprop2', '<=', 'bnother string']]],
                true,
            ],
            '<= string fails' => [
                $row1,
                ['criteria' => [['stringprop2', '<=', 'aaother string']]],
                false,
            ],
            '<= string passes when equal' => [
                $row1,
                ['criteria' => [['stringprop2', '<=', 'another string']]],
                true,
            ],
            '> int passes' => [
                $row1,
                ['criteria' => [['intprop1', '>', 10]]],
                true,
            ],
            '> int fails' => [
                $row1,
                ['criteria' => [['intprop1', '>', 12]]],
                false,
            ],
            '> int fails when equal' => [
                $row1,
                ['criteria' => [['intprop1', '>', 11]]],
                false,
            ],
            '> string passes' => [
                $row1,
                ['criteria' => [['stringprop2', '>', 'aaother string']]],
                true,
            ],
            '> string fails' => [
                $row1,
                ['criteria' => [['stringprop2', '>', 'bnother string']]],
                false,
            ],
            '> string fails when equal' => [
                $row1,
                ['criteria' => [['stringprop2', '>', 'another string']]],
                false,
            ],
            '>= int passes' => [
                $row1,
                ['criteria' => [['intprop1', '>=', 10]]],
                true,
            ],
            '>= int fails' => [
                $row1,
                ['criteria' => [['intprop1', '>=', 12]]],
                false,
            ],
            '>= int passes when equal' => [
                $row1,
                ['criteria' => [['intprop1', '>=', 11]]],
                true,
            ],
            '>= string passes' => [
                $row1,
                ['criteria' => [['stringprop2', '>=', 'aaother string']]],
                true,
            ],
            '>= string fails' => [
                $row1,
                ['criteria' => [['stringprop2', '>=', 'bnother string']]],
                false,
            ],
            '>= string passes when equal' => [
                $row1,
                ['criteria' => [['stringprop2', '>=', 'another string']]],
                true,
            ],
            'in int passes' => [
                                $row1,
                ['criteria' => [['intprop1', 'in', '56|11|87']]],
                true,
            ],
            'in int fails' => [
                $row1,
                ['criteria' => [['intprop2', 'in', '56|-35.6|17']]],
                false,
            ],
            '!in int passes' => [
                $row1,
                ['criteria' => [['intprop1', '!in', '0.56']]],
                true,
            ],
            '!in int fails' => [
                $row1,
                ['criteria' => [['intprop2', '!in', '-78.4|-35']]],
                false,
            ],
            'in string passes' => [
                $row1,
                ['criteria' => [['stringprop1', 'in', 'test|another|a string']]],
                true,
            ],
            'in string fails' => [
                $row1,
                ['criteria' => [['stringprop2', 'in', 'other string|test|again -5, str']]],
                false,
            ],
            '!in string passes' => [
                $row1,
                ['criteria' => [['stringprop1', '!in', 'str1|some another str']]],
                true,
            ],
            '!in string fails' => [
                $row1,
                ['criteria' => [['stringprop2', '!in', 'a test|another|another string|567']]],
                false,
            ],
            'multiple criteria, both true' => [
                $row1,
                ['criteria' => [
                    ['intprop1', '=', 11],
                    ['stringprop1', '=', 'a string'],
                ]
                ],
                true,
            ],
            'multiple criteria, first false' => [
                $row1,
                ['criteria' => [
                    ['intprop1', '!=', 11],
                    ['stringprop1', '=', 'a string'],
                ]
                ],
                false,
            ],
            'multiple criteria, second false' => [
                $row1,
                ['criteria' => [
                    ['intprop1', '=', 11],
                    ['stringprop1', '!=', 'a string'],
                ]
                ],
                false,
            ],
            'multiple criteria, both false' => [
                $row1,
                ['criteria' => [
                    ['intprop1', '!=', 11],
                    ['stringprop1', '!=', 'a string'],
                ]
                ],
                false,
            ],
        ];
    }

    /**
     * Test UnrecognizedOperator exception on filter().
     */
    public function testUnrecognizedOperator() : void
    {
        $record = $this->createMock(Record::class);
        $configuration = ['criteria' => [
            ['intprop1', '<==>', 5],
        ]];
        /** @var \Soong\Contracts\Filter\Filter $filter */
        $filter = new $this->filterClass($configuration);
        /** @var \Soong\Contracts\Data\Record $record */
        $this->expectException(UnrecognizedOperator::class);
        $this->expectExceptionMessage('Select filter: unrecognized operator <==>');
        $filter->filter($record);
    }
}
